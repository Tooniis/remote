// SPDX-License-Identifier: GPL-3.0-or-later

module remote;

import std.stdio;
import std.string;
import std.file;

import gio.Application: GioApplication = Application;
import gtk.Application;
import gtk.ApplicationWindow;
import gtk.Builder;
import gtk.Button;
import gtk.Widget;
import gtk.CssProvider;
import gtk.StyleContext;

import button;

import lirc.Client;

/* TODO: Load stylesheet from file */ 
const string css = ".button_power {\n\tcolor: #ff0000;\n}";

class Remote: Application
{
	const string title = "Remote";
	const string prog_name = "org.remote.Remote";

	Builder builder;
	CssProvider css_provider;
	ApplicationWindow window;
	int lirc_fd;

	const string[] button_names = [
		"button_power",
		"button_menu",
		"button_ok",
		"button_up",
		"button_down",
		"button_left",
		"button_right",
		"button_exit",
	];
	string[string] key_codes;

	RemoteButton[] buttons;

	this(string applicationId, GApplicationFlags flags)
	{
		super(applicationId, flags);

		super.addOnActivate(&this.initWindow);

		/* Create a builder */
		builder = new Builder();

		/* Create a CSS provider */
		css_provider = new CssProvider();

		/* Initialize LIRC */
		/* TODO: move toStringz to binding */
 		lirc_fd = lirc_init(toStringz(prog_name), 4);

		/* Map key codes */
		/*
		 * TODO: Initialize this array statically when it is implemented in D
		 * https://dlang.org/spec/hash-map.html#static_initialization
		 */
		key_codes["button_power"]	= "KEY_POWER";
		key_codes["button_menu"]	= "KEY_MENU";
		key_codes["button_ok"]		= "KEY_OK";
		key_codes["button_up"]		= "KEY_UP";
		key_codes["button_down"]	= "KEY_DOWN";
		key_codes["button_left"]	= "KEY_LEFT";
		key_codes["button_right"]	= "KEY_RIGHT";
		key_codes["button_exit"]	= "KEY_EXIT";
	}

	~this()
	{
		/* Release LIRC */
		lirc_deinit();
	}

	void initWindow(GioApplication a) {
		/* Load layout to new builder */
		builder.addFromFile("remote.glade");

		/* Load stylesheet */
		css_provider.loadFromData(css);

		/* Load window from builder */
		window = cast(ApplicationWindow) builder.getObject("window");
		if(!window) {
			printf("window not found\n");
			return;
		}

		foreach(string name; button_names) {
			Button button = cast(Button) builder.getObject(name);
			if (!button) {
				writeln("Failed to find", name);
				continue;
			}
			RemoteButton remote_button = new RemoteButton(
				name,
				cast(Button) builder.getObject(name),
				lirc_fd,
				cast(string)key_codes[name]
			);

			StyleContext context = remote_button.button.getStyleContext();
			context.addProvider(css_provider, GTK_STYLE_PROVIDER_PRIORITY_USER);
			buttons ~= remote_button;
		}

		window.setTitle(title);
		window.setApplication(this);
		window.show();
	}
}