// SPDX-License-Identifier: GPL-3.0-or-later

module button;

import std.stdio;
import std.string;

import gtk.Button;

import lirc.Client;

class RemoteButton {
	int lirc_fd;
	Button button;
	string name;
	string code;

	this(string name, Button button, int lirc_fd, string code) {
		this.lirc_fd = lirc_fd;
		/* Assign GtkButton to self */
		this.button = button;
		/* TODO: possibly use code as name */
		this.name = name;
		this.code = code;

		this.button.addOnClicked(&onPressed);
	}

	void onPressed(Button aux) {
		int ret;

		writeln(code);

		/* TODO: remove hard-coded tclmanual remote name */
		ret = lirc_send_one(lirc_fd, "tclmanual", toStringz(code));
		/* TODO: Better error signaling to user */
		if (ret)
			writeln("Failed to send code");
		else
			writeln("Sent code");
	}
}