// SPDX-License-Identifier: GPL-3.0-or-later

import std.stdio;

import gtk.Application;

import remote;

int main(string[] args)
{
	Remote app = new Remote(null, GApplicationFlags.FLAGS_NONE);

	return app.run(args);
}
